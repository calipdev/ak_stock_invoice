# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Aktiva (<http://aktiva.com.mx>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Stock and Invoice l10n_mx_facturae',
    'version': '1.0',
    'category': 'Hidden',
    'summary': 'Invoicing Control',
    'description': """
Manage invoices
==================================

This module override and set some mexican localization's partner fields up by default on the invoice when the invoice is created through the stock_picking.

Preferences
-----------
* Invoicing: choose how invoices will be paid

You need to choose the  invoicing method:

* *Before Delivery*: A Draft invoice is created and must be paid before delivery
""",
    'author': 'OpenERP SA',
    'website': 'http://www.openerp.com',
    'depends': ['stock', 'l10n_mx_facturae'],
    'data': [],
    'installable': True,
    'auto_install': True,
    
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
